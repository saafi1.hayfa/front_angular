#STAGE1
FROM node:12.11.1 as builder
# Set work directory
WORKDIR /app

# Install dependencies
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN  npm run build --prod --aot --build-optimizer --no-progress --output-path=./dist
RUN ls -la /app
RUN ls -la /app/dist
#STAGE 2
FROM nginx:latest 
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf 
EXPOSE 80 
ENTRYPOINT ["nginx", "-g", "daemon off;"]